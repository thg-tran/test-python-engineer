FROM python:3.7-slim-buster AS compile-image

WORKDIR /server/

# RUN apt-get update && apt-get install -y --no-install-recommends wget

# Install Poetry
# RUN wget -O - https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
#     cd /usr/local/bin && \
#     ln -s /opt/poetry/bin/poetry && \
#     poetry config virtualenvs.in-project true

COPY .poetry /opt/poetry
RUN ln -s /opt/poetry/bin/poetry && \
    /opt/poetry/bin/poetry config virtualenvs.in-project true

# RUN apt-get update && apt-get install -y --no-install-recommends build-essential && rm -rf /var/lib/apt/lists/*

# Copy poetry.lock* in case it doesn't exist in the repo
COPY ./pyproject.toml ./poetry.lock* /server/

# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then /opt/poetry/bin/poetry install --no-root ; else /opt/poetry/bin/poetry install --no-root --no-dev ; fi"

FROM python:3.7-slim-buster AS backend

WORKDIR /server/

# Copy Python dependencies from build image
COPY --from=compile-image /server/.venv /server/.venv

# Copy source
COPY ./scripts /server/scripts
COPY ./alembic /server/alembic
COPY alembic.ini /server
COPY ./app /server/app
ENV PYTHONPATH=/server

# Use python from venv
ENV PATH="/server/.venv/bin:$PATH"

# command to run on container start
ENTRYPOINT [ "/server/scripts/entrypoint.sh" ]
