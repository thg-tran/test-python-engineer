from math import floor
from typing import Optional, TypeVar, List
from ipaddress import ip_address, ip_network

_T = TypeVar('_T')

def get_middle_element(l: List[_T]) -> _T:
  return l[floor(len(l)/2)]

def check_reverse_string(s1: str, s2: str) -> bool:
  return s1 == s2[::-1]

def check_ip(ip: str, cird_list: List[str]) -> Optional[str]:
  for cird in cird_list:
    if ip_address(ip) in ip_network(cird):
      return cird
  return None
