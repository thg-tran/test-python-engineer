#!/bin/sh

set -e

bash scripts/prestart.sh

uvicorn --host 0.0.0.0 "$@" app.main:app
