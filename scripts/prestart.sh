#! /usr/bin/env bash

# Let the DB start
python /server/app/backend_pre_start.py

# Run migrations
# aerich upgrade

# Create initial data in DB
python /server/app/initial_data.py
