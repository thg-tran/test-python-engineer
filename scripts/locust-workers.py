import sys
from os.path import abspath
from subprocess import call
from threading import Thread
from time import sleep
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

if sys.platform == 'win32':
    DEFAULT_PATH = './.venv/Scripts/locust'
else:
    DEFAULT_PATH = './.venv/bin/locust'

running = True
def run_slave(*args, **kwargs):
    while running:
        call(*args, **kwargs)

def run_locust(locust_path=DEFAULT_PATH, number_slave=2, locustfile='./locustfile.py'):
    locust_path = abspath(locust_path)
    locustfile = abspath(locustfile)

    threads = [
        Thread(
            target=call,
            args=('"{}" --web-host "127.0.0.1" -f "{}" --master'.format(locust_path, locustfile),),
            kwargs={'shell': True}
        )
    ]

    for _ in range(number_slave):
        threads.append(Thread(
            target=run_slave,
            args=('"{}" -f "{}" --worker'.format(locust_path, locustfile),),
            kwargs={'shell': True}
        ))

    for t in threads:
        t.start()

    try:
        while True:
            sleep(5)
    except KeyboardInterrupt:
        pass

    global running
    running = False
    for t in threads:
        t.join()

def main(argv):
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description=('Run locust workers'),
        add_help=True,
    )
    parser.add_argument(
        '-p', "--locust-path",
        metavar='path', type=str,
        default= run_locust.__defaults__[0],
        help='Locust execution path'
    )
    parser.add_argument(
        '-s', "--number-slave",
        metavar='num', type=int,
        default= run_locust.__defaults__[1],
        help='Number of slaves'
    )
    parser.add_argument(
        '-f', "--locustfile",
        metavar='LOCUSTFILE', type=str,
        default= run_locust.__defaults__[2],
        help="Python module file to import, e.g. '../other.py'"
    )

    kwargs = vars(parser.parse_args(argv))
    run_locust(**kwargs)

if __name__ == '__main__':
    main(sys.argv[1:])
