from random import randint
from threading import Thread
from asyncio import get_event_loop, new_event_loop, set_event_loop

from locust import HttpUser, task, between
from websockets import connect

class MyUser(HttpUser):
    wait_time = between(0.1, 0.2)

    @task(50)
    def get_film(self):
        self.client.get(f"/api/v1/films/{randint(1, 10)}")

    @task(50)
    def get_films(self):
        self.client.get("/api/v1/films/?with_price=false&is_bought=false")

    @task(8)
    def get_bought_films(self):
        self.client.get(
            "/api/v1/films/?with_price=true&is_bought=true",
            headers=dict(
                Token=str(self.user_id),
            ),
        )

    @task(3)
    def get_bills(self):
        self.client.get(
            '/api/v1/bills/',
            headers=dict(
                Token=str(self.user_id),
            ),
        )

    @task(1)
    def buy_film(self):
        self.client.post(
            '/api/v1/bills/',
            json=dict(
                price_id=randint(1, 30)
            ),
            headers=dict(
                Token=str(self.user_id),
            ),
        )
        self.bought_count += 1

    def on_start(self):
        self.user_id = randint(1, 999999)
        self.client.get(f'/api/v1/users/{self.user_id}')
        self.bought_count = 0
        self.event_count = 0

        # self.thr = Thread(target=run_handler, args=(self,))
        # self.thr.start()

    def on_stop(self):
        if (getattr(self, 'thr', None)) is None: return

        self.thr.join()

        if self.bought_count == 0 and self.event_count == 0: return

        with open(f'./.data/{self.user_id}.log', 'w') as f:
            f.write(f'{self.bought_count == self.event_count}\nevent_count {self.event_count}\nbought_count {self.bought_count}')

def run_handler(user):
    try:
        loop = get_event_loop()
    except RuntimeError:
        loop = new_event_loop()
        set_event_loop(loop)

    loop.run_until_complete(connect_ws(user))

async def connect_ws(user):
    url = user.host.replace('http://', '')
    async with connect(f'ws://{url}/api/v1/ws/{user.user_id}', extra_headers=dict(token=str(user.user_id))) as ws:
        try:
            while True:
                await ws.recv()
                user.event_count += 1
        except:
            pass
