import enum
from typing import TYPE_CHECKING

from tortoise.models import Model
from tortoise import fields

if TYPE_CHECKING:
    from app.models import Price, Film, User

class BillStatus(enum.IntEnum):
    waiting = 0
    success = 1
    fail = 2

class Bill(Model):
    id = fields.IntField(pk=True)
    status = fields.IntEnumField(BillStatus)

    price_id: int
    price: fields.ForeignKeyRelation['Price'] = fields.ForeignKeyField(
        "models.Price",
    ) # type: ignore
    film_id: int
    film: fields.ForeignKeyRelation['Film'] = fields.ForeignKeyField(
        "models.Film",
    ) # type: ignore
    user_id: int
    user: fields.ForeignKeyRelation['User'] = fields.ForeignKeyField(
        "models.User",
    ) # type: ignore

