from typing import TYPE_CHECKING

from tortoise.models import Model
from tortoise import fields

if TYPE_CHECKING:
    from app.models import Film

class User(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
