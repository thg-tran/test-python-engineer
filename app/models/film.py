import enum
from typing import TYPE_CHECKING

from tortoise.models import Model
from tortoise import fields

if TYPE_CHECKING:
    from app.models import Price, User

class FilmType(enum.IntEnum):
    have_fee = 0
    free = 1
    avod = 2

class Film(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
    type = fields.IntEnumField(FilmType)

    prices: fields.ReverseRelation['Price']
    users: fields.ManyToManyRelation['User'] = fields.ManyToManyField(
        "models.User",
        through='bill',
        forward_key='user_id',
        backward_key='film_id',
    )
