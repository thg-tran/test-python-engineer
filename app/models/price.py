import enum
from typing import TYPE_CHECKING

from tortoise.models import Model
from tortoise import fields

if TYPE_CHECKING:
    from app.models import Film

class PriceType(enum.IntEnum):
    sell_through = 0
    tvod = 1
    svod = 2

class Price(Model):
    id = fields.IntField(pk=True)
    type = fields.IntEnumField(PriceType)
    unit_price = fields.IntField()

    film_id: int
    film: fields.ForeignKeyRelation['Film'] = fields.ForeignKeyField(
        "models.Film"
    ) # type: ignore
