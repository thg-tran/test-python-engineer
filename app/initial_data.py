import logging
from asyncio import run

from app.db.init_db import init_db
from app.db.session import setup_db_connection
from app.core.logger import logger


async def init() -> None:
    await setup_db_connection()
    await init_db()


def main() -> None:
    logger.info("Creating initial data")
    run(init())
    logger.info("Initial data created")


if __name__ == "__main__":
    main()
