from pydantic import BaseModel

from app.models.price import PriceType

# Shared properties
class PriceBase(BaseModel):
    unit_price: int
    type: PriceType
    film_id: int

# Properties to receive on creation
class PriceCreate(PriceBase):
    pass


# Properties to receive on update
class PriceUpdate(PriceBase):
    pass


# Properties shared by models stored in DB
class PriceInDBBase(PriceBase):
    id: int

    class Config:
        orm_mode = True
