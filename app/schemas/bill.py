from pydantic import BaseModel

from app.models.bill import BillStatus

# Shared properties
class BillBase(BaseModel):
    pass

# Properties to receive on creation
class BillCreate(BillBase):
    user_id: int
    film_id: int
    price_id: int
    status: BillStatus

class BillInput(BillBase):
    price_id: int

# Properties to receive on update
class BillUpdate(BillBase):
    status: BillStatus


# Properties shared by models stored in DB
class BillInDBBase(BillBase):
    id: int
    user_id: int
    film_id: int
    price_id: int
    status: BillStatus

    class Config:
        orm_mode = True
