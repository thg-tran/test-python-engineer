
from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise import Tortoise

from app.models import User, Bill, Film, Price

Tortoise.init_models(["app.models"], "models")

UserM = pydantic_model_creator(User, name='User', exclude=('films', 'bills'))
BillM = pydantic_model_creator(Bill, name='Bill', exclude=('price', 'film', 'user', 'users', 'films'))
FilmM = pydantic_model_creator(Film, name='Film', exclude=('users', 'bills', 'prices'))
FilmPriceM = pydantic_model_creator(Film, name='Film', exclude=('users', 'bills', 'prices.bills'))
PriceM = pydantic_model_creator(Price, name='Price', exclude=('films', 'bills'))
