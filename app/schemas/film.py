from app.schemas.price import PriceInDBBase
from typing import List
from pydantic import BaseModel

from app.models.film import FilmType

# Shared properties
class FilmBase(BaseModel):
    name: str
    type: FilmType


# Properties to receive on creation
class FilmCreate(FilmBase):
    pass


# Properties to receive on update
class FilmUpdate(FilmBase):
    pass


# Properties shared by models stored in DB
class FilmInDBBase(FilmBase):
    id: int
    prices: List[PriceInDBBase]

    class Config:
        orm_mode = True
