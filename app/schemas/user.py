from pydantic import BaseModel

# Shared properties
class UserBase(BaseModel):
    pass

# Properties to receive on creation
class UserCreate(UserBase):
    id: int
    name: str


# Properties to receive on update
class UserUpdate(UserBase):
    pass


# Properties shared by models stored in DB
class UserInDBBase(UserBase):
    id: int
    name: str

    class Config:
        orm_mode = True
