# For a new basic set of CRUD operations you could just do

from .base import CRUDBase
from .crud_film import film
from app.models import Film, Bill, Price, User
from app.schemas.film import FilmCreate, FilmUpdate
from app.schemas.price import PriceCreate, PriceUpdate
from app.schemas.bill import BillCreate, BillUpdate
from app.schemas.user import UserCreate, UserUpdate

# film = CRUDBase[Film, FilmCreate, FilmUpdate](Film)
price = CRUDBase[Price, PriceCreate, PriceUpdate](Price)
bill = CRUDBase[Bill, BillCreate, BillUpdate](Bill)
user = CRUDBase[User, UserCreate, UserUpdate](User)
