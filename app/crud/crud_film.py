from typing import List

from app.crud.base import CRUDBase
from app.models.film import Film
from app.schemas.film import FilmCreate, FilmUpdate


class CRUDFilm(CRUDBase[Film, FilmCreate, FilmUpdate]):
    pass


film = CRUDFilm(Film)
