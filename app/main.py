
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from fastapi.responses import UJSONResponse

from app.api.api import api_router
from app.core.config import settings
from app.api.ws import active_service
from app.db.session import setup_db_connection

app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
    default_response_class=UJSONResponse,
)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router, prefix=settings.API_V1_STR)

@app.on_event("startup")
async def startup():
    await setup_db_connection()

    app.service_task = active_service()
    await app.service_task.asend(None)

@app.on_event("shutdown")
async def shutdown():
    await app.service_task.asend(None)
    await app.service_task.aclose()
    app.service_task = None
