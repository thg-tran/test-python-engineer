from fastapi import APIRouter

from app.api.endpoints import users, films, bills
from app.api import ws

api_router = APIRouter()
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(films.router, prefix="/films", tags=["films"])
api_router.include_router(bills.router, prefix="/bills", tags=["bills"])
api_router.include_router(ws.router, prefix='/ws',)
