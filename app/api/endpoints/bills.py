from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Query

from app.api.deps import check_user, get_current_user
from app.crud import bill, price
from app.models import User
from app.models.bill import Bill, BillStatus
from app.schemas.bill import BillCreate, BillInput
from app.api.ws import handle_bill
from app.schemas import BillM

router = APIRouter()

@router.get('/{id}', response_model=BillM)
async def get_by_id(
    id: int,
):
    """
    Get bill by ID.
    """
    bill_db = await bill.get(id=id)
    if not bill_db:
        raise HTTPException(status_code=404, detail="Item not found")
    return bill_db

@router.get('/', response_model=List[BillM])
async def get_multi(
    skip: int = Query(0),
    limit: int = Query(10),
    user: Optional[User] = Depends(check_user),
):
    """
    Get bills.
    """
    query= Bill.filter()\
        .offset(skip).limit(limit)
    if user:
        query = query.filter(user_id=user.id)

    return await query

@router.post('/', response_model=BillM)
async def create_bill(
    bill_info: BillInput,
    user: User = Depends(get_current_user),
):
    price_db = await price.get(id=bill_info.price_id)
    if price_db is None:
        raise HTTPException(status_code=404, detail="Price not found")

    bill_db = await bill.create(
        obj_in=BillCreate(
            status=BillStatus.waiting,
            price_id=price_db.id,
            user_id=user.id,
            film_id=price_db.film_id,
        )
    )

    await handle_bill(bill_db.id)

    return bill_db
