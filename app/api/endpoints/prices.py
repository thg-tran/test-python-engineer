
from typing import List
from fastapi import APIRouter, HTTPException, Header

from app.crud import price
from app.schemas import PriceM

router = APIRouter()

@router.get('/{id}', response_model=PriceM)
async def get_by_id(
    id: int,
):
    """
    Get price by ID.
    """
    price_db = await price.get(id=id)
    if not price_db:
        raise HTTPException(status_code=404, detail="Item not found")
    return price_db

@router.get('/', response_model=List[PriceM])
async def get_multi(
    skip: int = Header(0),
    limit: int = Header(10),
):
    return await price.get_multi(
        skip=skip,
        limit=limit,
    )
