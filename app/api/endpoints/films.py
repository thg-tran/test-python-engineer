from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Query

from app.models import Film
from app.api.deps import check_user
from app.crud import film
from app.models.user import User
from app.schemas import FilmM, FilmPriceM

router = APIRouter()

@router.get('/{id}', response_model=FilmPriceM)
async def get_by_id(
    id: int,
):
    """
    Get film by ID.
    """
    film_db = await film.get(id=id)
    if not film_db:
        raise HTTPException(status_code=404, detail="Item not found")

    await film_db.fetch_related('prices')

    return film_db

@router.get('/', response_model=List[FilmM])
async def get_multi(
    skip: int = Query(0),
    limit: int = Query(10),
    is_bought: bool = Query(False),
    user: Optional[User] = Depends(check_user),
):
    query = Film.filter().offset(skip).limit(limit)
    if is_bought and user:
        query = query.filter(users=user.id).distinct()

    data = await query
    return data
