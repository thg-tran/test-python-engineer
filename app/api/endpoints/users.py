
from fastapi import APIRouter

from app.crud import user
from app.api.ws import send_event_user
from app.schemas.user import UserCreate
from app.schemas import UserM

router = APIRouter()

@router.get('/{id}', response_model=UserM)
async def get_by_id(
    id: int,
):
    """
    Get user by ID.
    """
    user_db = await user.get(id=id)
    if not user_db:
        user_db = await user.create(
            obj_in=UserCreate(
                name='',
                id=id,
            ),
        )
    return user_db

@router.post('/send-event/{id}')
async def send_event(
    id: int,
):
    await send_event_user(id)
