from typing import Optional

from fastapi import Depends, HTTPException, status, Header

from app import crud
from app.models.user import User

async def check_user(
    token: Optional[int] = Header(None),
) -> Optional[User]:
    if token is None:
        return None

    return await crud.user.get(id=token)

async def get_current_user(
    token: int = Header(...),
) -> User:
    user = await check_user(token)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
    return user
