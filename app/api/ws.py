from typing import Dict, Optional
from asyncio import ensure_future
from concurrent.futures import CancelledError

from fastapi import APIRouter, WebSocket, Depends
from websockets import connect, WebSocketClientProtocol
from websockets.exceptions import ConnectionClosedOK
import ujson

from app.api.deps import check_user
from app.models.user import User
from app.core.logger import logger
from app.core.config import settings

users: Dict[int, WebSocket] = {}
ws_service: WebSocketClientProtocol = None

async def active_service():
    global ws_service
    logger.info('connet to ws serveice')
    async with connect(settings.QUEUE_SERVICE) as service:
        logger.info('ws service connected')

        ws_service = service
        f = ensure_future(listen_service(service))

        yield service

        logger.info('shutting down service')
        f.cancel()

    logger.info('disconnect from service is completed')
    yield

async def listen_service(service: WebSocketClientProtocol):
    try:
        while True:
            data = await service.recv()

            js: dict = ujson.loads(data)
            method = js.get('method')

            if method == 'event':
                ws = users.get(js['result']['user_id'])
                if ws is None:
                    logger.warning(f"user disconnect {js['result']['user_id']}")
                    continue

                logger.info(f'send event {data}')
                await ws.send_json(js)
            elif js.get('error'):
                logger.error(f"method {method} error: {js['error']}")
            else:
                logger.info(data)
    except Exception as e:
        t = type(e)
        if t == ConnectionClosedOK or t == CancelledError:
            logger.info(f'close listen')
        else:
            logger.exception(e)

async def handle_bill(bill_id: int):
    await ws_service.send(ujson.dumps(dict(
        method='handle_bill',
        params=[bill_id],
    )))

async def send_event_user(id: int):
    await ws_service.send(ujson.dumps(dict(
        method='send_event',
        params=[id],
    )))

router = APIRouter()

@router.websocket('/{user_id}')
async def handler(
    user_id: int,
    ws: WebSocket,
    user: Optional[User] = Depends(check_user),
):
    if user is None and user_id is None:
        await ws.close()
        return
    if user:
        user_id = user.id

    await ws.accept()

    await ws_service.send(ujson.dumps(dict(
        method='add_user',
        params=[user_id],
    )))

    users[user_id] = ws
    try:
        while True:
            await ws.receive_text()
    except:
        pass
    finally:
        del users[user_id]

    await ws_service.send(ujson.dumps(dict(
        method='remove_user',
        params=[user_id],
    )))
