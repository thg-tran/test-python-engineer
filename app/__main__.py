from app.main import app

if __name__ == "__main__":
    from uvicorn import run

    run(app)
