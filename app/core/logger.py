from logging import getLogger, config

from uvicorn.config import LOGGING_CONFIG

LOGGING_CONFIG['formatters']['default']['fmt'] = '%(levelname)s: %(name)s - %(message)s'
LOGGING_CONFIG['loggers']['api'] = {"handlers": ["default"],"level": "INFO"}
config.dictConfig(LOGGING_CONFIG)

logger = getLogger('api')
