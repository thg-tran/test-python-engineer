from asyncio import Queue, sleep, run
from typing import Dict, Tuple

import ujson
from starlette.datastructures import Address
from websockets.server import Serve, WebSocketServerProtocol
from websockets.exceptions import ConnectionClosedOK

from app.db.session import setup_db_connection
from app.models.bill import BillStatus
from app.schemas.bill import BillUpdate
from app.crud import bill
from app.core.logger import logger

client: Dict[Address, WebSocketServerProtocol] = {}
users: Dict[int, Address] = {}

async def add_user(ws: WebSocketServerProtocol, user_id: int):
    users[user_id] = ws.remote_address
    await send_json(ws,
        dict(
            method='add_user',
            result='OK',
        )
    )

async def remove_user(ws: WebSocketServerProtocol, user_id: int):
    try:
        del users[user_id]
        await send_json(ws,
            dict(
                method='remove_user',
                result='OK',
            )
        )
    except Exception as e:
        await send_json(ws,
            dict(
                method='remove_user',
                error=str(e),
            )
        )

async def handle_bill(bill_queue: "Queue[Tuple[WebSocketServerProtocol, list]]"):
    while True:
        ws, data = await bill_queue.get()

        logger.info(f'handle bill {data[0]}')
        bill_db = await bill.get(data[0])
        if bill_db is None:
            await send_json(ws,
                dict(
                    method='handle_bill',
                    error=dict(
                        bill_id=data[0],
                        message='Bill not found',
                    ),
                )
            )
            continue

        # run some tasks
        await sleep(0.5)

        bill_db = await bill.update(
            db_obj=bill_db,
            obj_in=BillUpdate(
                status=BillStatus.success,
            ),
        )
        await send_json(ws,
            dict(
                method='handle_bill',
                result='OK',
            )
        )
        if users.get(bill_db.user_id) and client.get(users[bill_db.user_id]):
            await send_json(
                client[users[bill_db.user_id]],
                dict(
                    method='event',
                    result=dict(
                        user_id=bill_db.user_id,
                        status=BillStatus.success,
                        bill_id=bill_db.id,
                    ),
                )
            )

async def send_json(ws: WebSocketServerProtocol, data: dict):
    await ws.send(ujson.dumps(data))

async def ws_handler(
    bill_queue: "Queue[Tuple['WebSocketServerProtocol', list]]",
    ws: WebSocketServerProtocol,
    path: str,
):
    logger.info(f'connect from {ws.remote_address}')

    client[ws.remote_address] = ws
    try:
        while True:
            req: dict = ujson.loads(await ws.recv())
            logger.info(req)
            method = req.get('method')
            if method == 'add_user':
                await add_user(ws, req['params'][0])
            elif method == 'remove_user':
                await remove_user(ws, req['params'][0])
            elif method == 'handle_bill':
                await bill_queue.put((ws, req['params']))
            elif method == 'send_event':
                user_id = req['params'][0]
                if users.get(user_id) and client.get(users[user_id]):
                    await send_json(
                        client[users[user_id]],
                        dict(
                            method='event',
                            result=dict(
                                user_id=user_id,
                                data='test',
                            ),
                        )
                    )
    except Exception as e:
        if type(e) == ConnectionClosedOK:
            logger.info(f'connection close {ws.remote_address}')
        else:
            logger.exception(e)
    finally:
        client.pop(ws.remote_address)
        await ws.close()

async def main(host: str, port: int):
    await setup_db_connection()

    bill_queue = Queue()

    def handler(ws, path):
        return ws_handler(bill_queue, ws, path)

    logger.info(f'Start websocket at {host} {port}')
    server = Serve(
        handler,
        host=host,
        port=port,
    )
    await server

    try:
        await handle_bill(bill_queue)
    except Exception as e:
        logger.exception(e)

if __name__ == '__main__':
    from uvicorn.loops.auto import auto_loop_setup
    auto_loop_setup()

    host = '0.0.0.0'
    port = 5000

    run(main(host, port))
