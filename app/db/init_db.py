
from app import crud
from app.schemas.price import PriceCreate
from app.models.film import FilmType
from app.schemas.film import FilmCreate

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


async def init_db() -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)

    film_dbs = await crud.film.get_multi(limit=1)
    if len(film_dbs) == 0:
        for i in range(10):
            film_db = await crud.film.create(
                obj_in=FilmCreate(
                    name='',
                    type=FilmType.have_fee,
                )
            )
            for j in range(3):
                await crud.price.create(
                    obj_in=PriceCreate(
                        unit_price=i*j,
                        type=j,
                        film_id=film_db.id,
                    )
                )
