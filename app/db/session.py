
from tortoise import Tortoise

from app.core.config import settings

async def setup_db_connection():
    await Tortoise.init(
        db_url=settings.SQLALCHEMY_DATABASE_URI,
        modules={"models": ["app.models"]},
    )
    await Tortoise.generate_schemas()
